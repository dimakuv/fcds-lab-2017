/*
 * The Game of Life
 *
 * a cell is born, if it has exactly three neighbours
 * a cell dies of loneliness, if it has less than two neighbours
 * a cell dies of overcrowding, if it has more than three neighbours
 * a cell survives to the next generation, if it does not die of loneliness
 * or overcrowding
 *
 * In this version, a 2D array of ints is used.  A 1 cell is on, a 0 cell is off.
 * The game plays a number of steps (given by the input), printing to the screen each time.  'x' printed
 * means on, space means off.
 *
 */

/* DMITRII KUVAISKII: improvements
 *   - board is represented as a single bit-vector (before was char[][])
 *   - parallelized using OpenMP for-parallel loop
 */
#include <stdio.h>
#include <stdlib.h>

typedef unsigned long cellvector_t;
#define BITS ( sizeof(cellvector_t) * 8 )
#define SIZE(s) ( (s*s - 1) / BITS + 1 )
#define GET(board, s, i, j)   ( (board[(i*s + j) / BITS] & (1ul << ((i*s + j) % BITS))) != 0 )
#define SET(board, s, i, j)   ( board[(i*s + j) / BITS] |= (1ul << ((i*s + j) % BITS)) )
#define UNSET(board, s, i, j) ( board[(i*s + j) / BITS] &= ~(1ul << ((i*s + j) % BITS)) )

cellvector_t* allocate_board(int size) {
    return (cellvector_t*) malloc(sizeof(cellvector_t) * SIZE(size));
}

void free_board(cellvector_t* board) {
    free(board);
}

/* return the number of on cells adjacent to the i,j cell */
int adjacent_to(cellvector_t* board, int size, int i, int j) {
    int sk = (i>0) ? i-1 : i;
    int ek = (i+1 < size) ? i+1 : i;
    int sl = (j>0) ? j-1 : j;
    int el = (j+1 < size) ? j+1 : j;

    int count=0;
    for (int k=sk; k<=ek; k++)
        for (int l=sl; l<=el; l++)
            count += GET(board, size, k, l);
    count -= GET(board, size, i, j);
    return count;
}

/* for each cell, apply the rules of Life */
void play(cellvector_t* board, cellvector_t* newboard, int size, int threads) {
    #pragma omp parallel for schedule(dynamic, 100) num_threads(threads)
    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++) {
            int a = adjacent_to(board, size, i, j);
            if (a == 2) {
                // if was populated, it survives; if not, nothing happens
                if ( GET(board, size, i, j) )  SET(newboard, size, i, j);
                else                           UNSET(newboard, size, i, j);
            }
            else if (a == 3) {
                // if was populated, it survives; if not, it becomes populated
                SET(newboard, size, i, j);
            }
            else {
                // other cases are a<2 or a>3 -- cell dies
                UNSET(newboard, size, i, j);
            }
        }
}

/* print the life board */
void print(cellvector_t* board, int size) {
    for (int j=0; j<size; j++) {
        for (int i=0; i<size; i++)
            printf("%c", GET(board, size, i, j) ? 'x' : ' ');
        printf("\n");
    }
}

/* read a file into the life board */
void read_file(FILE* f, cellvector_t* board, int size) {
    // TODO: what is this 10?!
    char *s = (char *) malloc(size+10);
    for (int j=0; j<size; j++) {
        fgets (s, size+10, f);
        for (int i=0; i<size; i++) {
            if (s[i] == 'x')  SET(board, size, i, j);
            else              UNSET(board, size, i, j);
        }
    }
}

int main() {
    int threads = 1;
    if (getenv("MAX_CPUS"))
        threads = atoi(getenv("MAX_CPUS"));
    if (threads < 1) threads = 1;

    int size, steps;
    FILE* f = stdin;

    fscanf(f,"%d %d", &size, &steps);
    while (fgetc(f) != '\n') /* no-op */;

    cellvector_t* prev = allocate_board(size);
    read_file(f, prev, size);
    fclose(f);

    cellvector_t* next = allocate_board(size);
    cellvector_t* tmp;
#ifdef DEBUG
    printf("Initial \n");
    print(prev, size);
    printf("----------\n");
#endif

    for (int i=0; i<steps; i++) {
        play(prev, next, size, threads);
#ifdef DEBUG
        printf("%d ----------\n", i);
        print(next, size);
#endif
        tmp  = next;
        next = prev;
        prev = tmp;
    }

    print(prev, size);
    free_board(prev);
    free_board(next);
}
