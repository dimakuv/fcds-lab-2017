/* threadpool-based sudokount: simple extension of sequential sudokount
 *
 * On my Dell Inspiron laptop: `./sudokount 1 < sudokount2.in` is `59.02s`
 *                             `./sudokount 2 < sudokount2.in` is `31.01s`
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include "threadpool.h"

int MAXDEPTH=25;   // magic number to balance amount of work each thread does

#define INT_TYPE unsigned long long 
#define INT_TYPE_SIZE (sizeof(INT_TYPE) * 8)
#define CELL_VAL_SIZE 1
//MAX_BDIM = floor(sqrt(CELL_VAL_SIZE * INT_TYPE_SIZE)). Current value set for 64-bit INT_TYPE, adjust if needed
#define MAX_BDIM 8

enum SOLVE_STRATEGY {SUDOKU_SOLVE, SUDOKU_COUNT_SOLS};
#define SUDOKU_SOLVE_STRATEGY SUDOKU_COUNT_SOLS

#define BUILD_ERROR_IF(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
void BUILD_TIME_CHECKS() {
    BUILD_ERROR_IF(INT_TYPE_SIZE * CELL_VAL_SIZE < MAX_BDIM * MAX_BDIM);
}

typedef struct cellval {
    /* assuming CELL_VAL_SIZE=1, we can say that cell_v.v is a 64-bit int ->
     * v is a bit array where each set bit represents that bit-index number
     * is in the array (e.g., "0101" represents ["1", "3"])
     */
    INT_TYPE v[CELL_VAL_SIZE];
} cell_v;

typedef struct cell_coord {
    int r,c;
} cell_coord;

typedef struct sudoku {
    int bdim;
    int dim;
    int peers_size;
    int* grid;
    
    /* unit_list is a 3-dimensional array of cells
     * while peers is a 2-dimensional set of cells
     */
    cell_coord ****unit_list; //[r][c][0 - row, 1 - column, 2 - box],
    cell_coord ***peers;
    cell_v **values;
    
    unsigned long long sol_count;
} sudoku;

typedef struct threadargs {
    sudoku *s;
    cell_v **values;
    int I;
    int J;
    int K;
} threadargs;

threadpool_t *pool;
int tasks = 0, done = 0;

static int assign (sudoku *s, cell_v **values, int i, int j, int d);
void search_and_cleanup(sudoku *s, cell_v **values, int I, int J, int K, int depth);
void thread_search(void *arg);

/* in bit array *v.v, check p bit (i.e., check if number p is in set) */
static inline int cell_v_get(cell_v *v, int p) {
    return !!((*v).v[(p - 1) / INT_TYPE_SIZE] & (((INT_TYPE)1) << ((p - 1) % INT_TYPE_SIZE))); //!! otherwise p > 32 breaks the return
}

/* in bit array *v.v, unset p bit (i.e., remove number p from set) */
static inline void cell_v_unset(cell_v *v, int p) {
    (*v).v[(p - 1) / INT_TYPE_SIZE] &= ~(((INT_TYPE)1) << ((p - 1) % INT_TYPE_SIZE));
}

/* in bit array *v.v, set p bit (i.e., add number p in set) */
static inline void cell_v_set(cell_v *v, int p) {
    (*v).v[(p - 1) / INT_TYPE_SIZE] |= ((INT_TYPE)1) << ((p -1) % INT_TYPE_SIZE);
}

/* in bit array *v.v, count number of set bits (i.e., count size of set) */
static inline int cell_v_count(cell_v *v) {
    int acc = 0;
    for (int i = 0; i < CELL_VAL_SIZE; i++) 
        acc += __builtin_popcountll((*v).v[i]);
    return acc;
}

/* in bit array *v.v, only if it has one number in set, return this number */
static inline int digit_get (cell_v *v) {
    int count = cell_v_count(v);
    if (count != 1) return -1;
    for (int i = 0; i < CELL_VAL_SIZE; i++) 
        if ((*v).v[i]) return 1 + INT_TYPE_SIZE * i + __builtin_ctzll((*v).v[i]);
    return -1;
}

static void destroy_sudoku(sudoku *s) {
    for (int i = 0; i < s->dim; i++) {
        for (int j = 0; j < s->dim; j++) {
            for (int k = 0; k < 3; k++)
                free(s->unit_list[i][j][k]);
            free(s->unit_list[i][j]);
        }
        free(s->unit_list[i]);
    }
    free(s->unit_list);
    
    for (int i = 0; i < s->dim; i++) {
        for (int j = 0; j < s->dim; j++)
            free(s->peers[i][j]);
        free(s->peers[i]);
    }
    free(s->peers);
    
    for (int i = 0; i < s->dim; i++) 
        free(s->values[i]);
    free(s->values);
    
    free(s);
}

static void init(sudoku *s) {
    int i, j, k, l, pos;
    
    //unit list 
    for (i = 0; i < s->dim; i++) {
        int ibase = i / s->bdim * s->bdim;
        for (j = 0; j < s->dim; j++) {
            for (pos = 0; pos < s->dim; pos++) {
                s->unit_list[i][j][0][pos].r = i; //row 
                s->unit_list[i][j][0][pos].c = pos;
                s->unit_list[i][j][1][pos].r = pos; //column
                s->unit_list[i][j][1][pos].c = j;
            }
            int jbase = j / s->bdim * s->bdim;
            for (pos = 0, k = 0; k < s->bdim; k++) //box
                for (l = 0; l < s->bdim; l++, pos++) {
                    s->unit_list[i][j][2][pos].r = ibase + k;
                    s->unit_list[i][j][2][pos].c = jbase + l;
                }
        }
    }
    
    //peers
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++) {
            pos = 0;
            for (k = 0; k < s->dim; k++) { //row
                if (s->unit_list[i][j][0][k].c != j)
                    s->peers[i][j][pos++] = s->unit_list[i][j][0][k]; 
            }
            // TODO: why does `row` has separate loop?
            for (k = 0; k < s->dim; k++) { 
                cell_coord sq = s->unit_list[i][j][1][k]; //column
                if (sq.r != i)
                    s->peers[i][j][pos++] = sq; 
                sq = s->unit_list[i][j][2][k]; //box
                if (sq.r != i && sq.c != j)
                    s->peers[i][j][pos++] = sq; 
            }
        }
    assert(pos == s->peers_size);
}

/* return 0 if grid is invalid (one unit has two same numbers) */
static int parse_grid(sudoku *s) {
    int i, j, k;
    int ld_vals[s->dim][s->dim];
    for (k = 0, i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++, k++) {
            ld_vals[i][j] = s->grid[k];
        }
    
    /* set all bits in bit-array s->values[i][j] representing
     * all possible numbers available in this sudoku
     */
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++)
            for (k = 1; k <= s->dim; k++)
                cell_v_set(&s->values[i][j], k);
    
    /* check that no one unit has two same numbers */
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++)
            if (ld_vals[i][j] > 0 && !assign(s, s->values, i, j, ld_vals[i][j]))
                return 0;

    return 1;
}

/* bdim is box-side size (3 for usual sudoku) */
static sudoku *create_sudoku(int bdim, int *grid) {
    assert(bdim <= MAX_BDIM);
    
    sudoku *r = malloc(sizeof(sudoku));
    r->bdim = bdim;
    int dim = bdim * bdim; /* dim is row/column length */
    r->dim = dim;
    r->peers_size = 3 * dim - 2 * bdim - 1; /* -1 for myself, -2*bdim for subrow & subcolumn in box */
    r->grid = grid;
    r->sol_count = 0;
    
    //unit_list item is [r][c][0 - row, 1 - column, 2 - box][ix]
    r->unit_list = malloc(sizeof(cell_coord***) * dim);
    assert(r->unit_list);
    for (int i = 0; i < dim; i++) {
        r->unit_list[i] = malloc(sizeof(cell_coord**) * dim);
        assert (r->unit_list[i]);
        for (int j = 0; j < dim; j++) {
            r->unit_list[i][j] = malloc(sizeof(cell_coord*) * 3);
            assert(r->unit_list[i][j]);
            for (int k = 0; k < 3; k++) {
                r->unit_list[i][j][k] = calloc(dim, sizeof(cell_coord));
                assert(r->unit_list[i][j][k]);
            }
        }
    }
    
    //peers item is [r][c][ix]
    r->peers = malloc(sizeof(cell_coord**) * dim);
    assert(r->peers);
    for (int i = 0; i < dim; i++) {
        r->peers[i] = malloc(sizeof(cell_coord*) * dim);
        assert(r->peers[i]);
        for (int j = 0; j < dim; j++) {
            r->peers[i][j] = calloc(r->peers_size, sizeof(cell_coord));
            assert(r->peers[i][j]);
        }
    }
    
    //values item is a bit-array in [r][c]
    r->values = malloc (sizeof(cell_v*) * dim);
    assert(r->values);
    for (int i = 0; i < dim; i++) {
        r->values[i] = calloc(dim, sizeof(cell_v));
        assert(r->values[i]);
    }
    
    /* now that we created unit_list and peers, init them */
    init(r);
    /* now that we created values, init them with known numbers in grid */
    if (!parse_grid(r)) {
        printf("Error parsing grid\n");
        destroy_sudoku(r);
        return 0;
    }
    
    return r;
}

/* -------------------- algorithm starts -------------------- */
/* eliminate number d in values[i][j] & propagate the change to peers;
 * return 0 if contradiction is detected (e.g., eliminated all values in me)
 */
static int eliminate (sudoku *s, cell_v **values, int i, int j, int d) {
    int k, ii, cont, pos;
    
    /* d was already eliminated in this cell, nothing to be done */
    if (!cell_v_get(&values[i][j], d)) 
        return 1;

    cell_v_unset(&values[i][j], d);

    int count = cell_v_count(&values[i][j]);
    if (count == 0) {
        /* after eliminating d, nothing is left in cell -> contradiction! */
        return 0;
    } else if (count == 1) {
        // --- strategy 1 ---
        /* after eliminating d, only one number is left in cell ->
         * this cell is "solved", eliminate remaining number from peers
         * (if found contradiction in any of peers -> panic!)
         */
        for (k = 0; k < s->peers_size; k++)  // O(???)
            if (!eliminate(s, values, s->peers[i][j][k].r, s->peers[i][j][k].c, digit_get(&values[i][j])))
                return 0;
    }

    // --- strategy 2 ---
    /* at this point, we eliminated d from me and possibly from peers ->
     * check my three units: if only one cell in unit has d in its values,
     *                       then d is "solution" of this cell
     */
    for (k = 0; k < 3; k++) {//row, column, box 
        cont = 0;
        pos = 0;
        cell_coord* u = s->unit_list[i][j][k];
        /* count the number of cells in unit having d as possible solution */
        for (ii = 0; ii < s->dim; ii++) {
            if (cell_v_get(&values[u[ii].r][u[ii].c], d)) {
                cont++;
                pos = ii;
            }
        }
        /* no cell in unit has d as possible solution -> impossible, contradiction! */
        if (cont == 0)
            return 0;
        else if (cont == 1) {
            /* exactly one cell in unit has d as possible solution -> it is the solution */
            if (!assign(s, values, u[pos].r, u[pos].c, d))  // O(???)
                return 0;
        }
    }
    return 1;
}

/* eliminate all numbers in values[i][j] except for known d,
 * propagate the change to all peers; return 0 if contradiction 
 * is detected (e.g., eliminated all values in me or in one of peers)
 */
static int assign (sudoku *s, cell_v **values, int i, int j, int d) {
    for (int d2 = 1; d2 <= s->dim; d2++)
        if (d2 != d) 
            if (!eliminate(s, values, i, j, d2))  // O(???)
               return 0;
    return 1;
}

/* recursive search by trying all numbers in one cell (chosen using "min remaining values" heuristic)
 *   parallel task-based threadpool implementation:
 *     - call search() recursively until we reach depth of MAXDEPTH
 *       (this specifies the amount of work one task performs)
 *     - when we reach MAXDEPTH, create new tasks instead of search() call
 *         - allocate and fill new `values` array and pass as task argument
 *         - if task cannot be dispatched due to too-many pending tasks,
 *           back-off by setting depth = 0 and continue work in this thread
 */
static void search (sudoku *s, cell_v **values, int I, int J, int K, int depth) {
    int i, j, k;
    depth++;

    if (K && !assign(s, values, I, J, K)) {
        // cannot assign, this combination is unsolvable
        return;
    }

    /* if all cells in grid contain exactly one number,
     * then there is a solution for each cell -> solved */
    // TODO PERF: `solved` flag can be incorporated in `assign` and `eliminate`
    int solved = 1;
    for (i = 0; solved && i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) 
            if (cell_v_count(&values[i][j]) != 1) {
                solved = 0;
                break;
            }
    if (solved) {
        __sync_fetch_and_add(&s->sol_count, 1);   // atomic s->sol_count++
        return;
    }

    //ok, there is still some work to be done
    int min = INT_MAX;
    int minI = -1;
    int minJ = -1;
    
    /* "minimum remaining values" heuristic: choose which cell to explore next */
    // TODO PERF: `min` cell can be incorporated in `assign` and `eliminate`
    for (i = 0; i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) {
            int used = cell_v_count(&values[i][j]);
            if (used > 1 && used < min) {
                min = used;
                minI = i;
                minJ = j;
            }
        }

    /* try all possible numbers in chosen cell:
     * assign each number to this cell and recurse
     */
    for (k = 1; k <= s->dim; k++) {
        if (cell_v_get(&values[minI][minJ], k))  {
            // create new values
            cell_v **values_new = malloc (sizeof (cell_v *) * s->dim);
            for (i = 0; i < s->dim; i++)
                values_new[i] = malloc (sizeof (cell_v) * s->dim);
            for (i = 0; i < s->dim; i++)
                for (j = 0; j < s->dim; j++)
                    values_new[i][j] = values[i][j];

            int spawnedtask = 0;
            if (depth > MAXDEPTH) {
                // add new search() as task in threadpool
                threadargs* args = (threadargs*) malloc(sizeof(threadargs));
                args->s = s;      args->values = values_new;
                args->I = minI;   args->J = minJ;
                args->K = k;
                __sync_fetch_and_add(&tasks, 1);
                spawnedtask = 1;

                if (threadpool_add(pool, &thread_search, args, 0) < 0) {
                    // threadpool's queue is full -> do work myself then
                    spawnedtask = 0;
                    depth = 0;
                    __sync_fetch_and_sub(&tasks, 1);
                }
            }

            if (!spawnedtask)
                search_and_cleanup(s, values_new, minI, minJ, k, depth);
        }
    }
}

void search_and_cleanup(sudoku *s, cell_v **values, int I, int J, int K, int depth) {
    int i;
    search(s, values, I, J, K, depth);
    for (i = 0; i < s->dim; i++)
        free(values[i]);
    free(values);
}

/* entry point for new tasks, executed by some worker thread
 * this task is responsible to free values array and args
 */
void thread_search(void *arg) {
    threadargs* args = (threadargs*) arg;
    search_and_cleanup(args->s, args->values, args->I, args->J, args->K, 0);
    free(args);
    __sync_fetch_and_add(&done, 1);   // atomic done++
}

/* main thread starts search() by spawning several tasks
 * immediately and waits until all tasks finish */
int solve(sudoku *s) {
    search(s, s->values, 0, 0, 0, MAXDEPTH+1);
    while (tasks > done) {
        sleep(1);
    }
    return 0;
}
/* -------------------- algorithm ends -------------------- */

static void display(sudoku *s) {
    printf("%d\n", s->bdim);
    for (int i = 0; i < s->dim; i++)
        for (int j = 0; j < s->dim; j++)
            printf("%d ",  digit_get(&s->values[i][j]));
}

int main (int argc, char **argv) {

    int size;
    assert(scanf("%d", &size) == 1);
    assert (size <= MAX_BDIM);
    int buf_size = size * size * size * size;
    int buf[buf_size];

    for (int i = 0; i < buf_size; i++) {
        if (scanf("%d", &buf[i]) != 1) {
            printf("error reading file (%d)\n", i);
            exit(1);
        }
    }

    if (argc >= 2) MAXDEPTH = atoi(argv[1]);

    int thread = 1;
    if (getenv("MAX_CPUS"))
        thread = atoi(getenv("MAX_CPUS"));
    if (thread < 1) thread = 1;
    pool = threadpool_create(thread, MAX_QUEUE, 0);

    sudoku *s = create_sudoku(size, buf);
    if (s) {
        solve(s);
        if (s->sol_count) {
            switch (SUDOKU_SOLVE_STRATEGY) {
                case SUDOKU_SOLVE:
                    display(s);
                    break;
                case SUDOKU_COUNT_SOLS: 
                    printf("%lld\n", s->sol_count);
                    break;
                default:
                    assert(0);
            }
        } else {
            printf("Could not solve puzzle.\n");
        }
        destroy_sudoku(s);
    } else {
        printf("Could not load puzzle.\n");
    }

    threadpool_destroy(pool, 0);
    return 0;
}
