# Performance experiments (single-threaded, original) #

$ perf stat ./sudokount < sudokount2.in

      55479,808956      task-clock (msec)         #    1,000 CPUs utilized          
               249      context-switches          #    0,004 K/sec                  
                 8      cpu-migrations            #    0,000 K/sec                  
               131      page-faults               #    0,002 K/sec                  
   168.915.985.482      cycles                    #    3,045 GHz                    
   <not supported>      stalled-cycles-frontend  
   <not supported>      stalled-cycles-backend   
   440.778.498.322      instructions              #    2,61  insns per cycle        
    51.160.499.162      branches                  #  922,146 M/sec                  
       961.398.569      branch-misses             #    1,88% of all branches        

$ perf stat -e L1-dcache-loads,L1-dcache-load-misses -e L1-dcache-stores,L1-dcache-store-misses -e LLC-loads,LLC-load-misses -e LLC-store-misses,LLC-stores  ./sudokount < sudokount2.in

   135.657.028.312      L1-dcache-loads                                               (42,86%)
     2.133.000.540      L1-dcache-load-misses     #    1,57% of all L1-dcache hits    (57,14%)
    42.981.138.476      L1-dcache-stores                                              (57,14%)
   <not supported>      L1-dcache-store-misses   
        42.923.771      LLC-loads                                                     (57,15%)
         1.717.423      LLC-load-misses           #    4,00% of all LL-cache hits     (57,15%)
           246.462      LLC-store-misses                                              (28,57%)
        16.778.101      LLC-stores                                                    (28,57%)


# Performance experiments (multithreaded, threadpool using pthread) #

$ ./cds-tool/bin/cds-tool run --measure --image 0f3decbddb05 -c 1,2,4 --input 11mopp/sudokount/sudokount2.in 11mopp-sudokount
program; run; cpus; duration;
11mopp-sudokount; 0; 1; 63018608
11mopp-sudokount; 0; 2; 30014986
11mopp-sudokount; 0; 4; 28004180
11mopp-sudokount; 1; 1; 58018280
11mopp-sudokount; 1; 2; 30015564
11mopp-sudokount; 1; 4; 28003939
11mopp-sudokount; 2; 1; 58018457
11mopp-sudokount; 2; 2; 30016744
11mopp-sudokount; 2; 4; 28004038

$ ./cds-tool/bin/cds-tool run --measure --image 0f3decbddb05 -c 1,2,4 --input 11mopp/sudokount/judge.in 11mopp-sudokount
program; run; cpus; duration;
11mopp-sudokount; 0; 1; 95064440
11mopp-sudokount; 0; 2; 49048125
11mopp-sudokount; 0; 4; 48060599

